<?php

use Illuminate\Database\Seeder;

class AccountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accounts')->truncate();
        // DB::table('accounts')->insert([
        //     'name' => 'Demo',
        //     'email' => 'demo@cargospace.ng',
        //     'password' => bcrypt('faker00tX'),
        // ],[
        //     'name' => 'Demo',
        //     'email' => 'demo@cargospace.ng',
        //     'password' => bcrypt('faker00tX'),
        // ]);
    }
}
