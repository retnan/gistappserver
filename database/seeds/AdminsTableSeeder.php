<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->truncate();
        DB::table('admins')->insert([[
            'name' => 'Preston Obadan',
            'email' => 'obadan@logicaladdress.com',
            'password' => bcrypt('faker00tX'),
        ],[
            'name' => 'Retnan Daser',
            'email' => 'dretnan@logicaladdress.com',
            'password' => bcrypt('faker00tX'),
        ]]);
    }
}
