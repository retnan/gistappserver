<?php

use Illuminate\Database\Seeder;

class DispatcherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dispatchers')->truncate();
        DB::table('dispatchers')->insert([[
            'name' => 'Dispatcher v1',
            'email' => 'dispatcher_v1@logicaladdress.com',
            'password' => bcrypt('faker00tX'),
        ],[
            'name' => 'Dispatcher v2',
            'email' => 'dispatcher_v2@logicaladdress.com',
            'password' => bcrypt('faker00tX'),
        ]]);
    }
}
