<?php

use Illuminate\Database\Seeder;

class FleetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fleets')->truncate();
        DB::table('fleets')->insert([[
            'name' => 'Fleet v1',
            'email' => 'fleet_v1@logicaladdress.com',
            'password' => bcrypt('faker00tX'),
        ],[
            'name' => 'Fleet v2',
            'email' => 'fleet_v2@logicaladdress.com',
            'password' => bcrypt('faker00tX'),
        ]]);
    }
}
